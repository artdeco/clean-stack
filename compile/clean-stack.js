#!/usr/bin/env node
'use strict';
const os = require('os');             
const c = os.homedir;
const d = /\s+at.*(?:\(|\s)(.*)\)?/, e = /^(?:(?:(?:node|(?:internal\/[\w/]*|.*node_modules\/(?:IGNORED_MODULES)\/.*)?\w+)\.js:\d+:\d+)|native)/;
module.exports = {$cleanStack:(f, g = {}) => {
  const {pretty:h = !1, ignoredModules:k = ["pirates", "@artdeco/pirates"]} = g, l = new RegExp(e.source.replace("IGNORED_MODULES", k.join("|")));
  return f.replace(/\\/g, "/").split("\n").filter(a => {
    a = a.match(d);
    if (null === a || !a[1]) {
      return !0;
    }
    a = a[1];
    return a.includes(".app/Contents/Resources/electron.asar") || a.includes(".app/Contents/Resources/default_app.asar") ? !1 : !l.test(a);
  }).filter(a => a.trim()).map(a => {
    if (h) {
      const m = c().replace(/\\/g, "/");
      return a.replace(/\s+at.*(?:\(|\s)(.*)\)?/, (n, b) => n.replace(b, b.replace(m, "~")));
    }
    return a;
  }).join("\n");
}};


//# sourceMappingURL=clean-stack.js.map