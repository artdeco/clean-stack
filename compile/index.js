const { $cleanStack } = require('./clean-stack')

/**
 * Remove internal Node.JS lines from the error stack traces.
 * @param {string} stack The stack to clean.
 * @param {!_cleanStack.Config} [options] Options for the program.
 * @param {boolean} [options.pretty=false] Replace the absolute path to the home directory with the `~`. Default `false`.
 * @param {!Array<string>} [options.ignoredModules="［'pirates'］"] Which modules to ignore in the path. Default `［'pirates'］`.
 * @return {string}
 */
function cleanStack(stack, options) {
  return $cleanStack(stack, options)
}

module.exports = cleanStack

/* typal types/index.xml namespace */
/**
 * @typedef {_cleanStack.Config} Config `＠record` Options for the program.
 * @typedef {Object} _cleanStack.Config `＠record` Options for the program.
 * @prop {boolean} [pretty=false] Replace the absolute path to the home directory with the `~`. Default `false`.
 * @prop {!Array<string>} [ignoredModules="［'pirates'］"] Which modules to ignore in the path. Default `［'pirates'］`.
 */
