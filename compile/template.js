const { $cleanStack } = require('./clean-stack')

/**
 * @methodType {_cleanStack.cleanStack}
 */
function cleanStack(stack, options) {
  return $cleanStack(stack, options)
}

module.exports = cleanStack

/* typal types/index.xml namespace */
