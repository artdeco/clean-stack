/**
 * @fileoverview
 * @externs
 */
/* typal types/index.xml */
/** @const */
var _cleanStack = {}
/**
 * Options for the program.
 * @record
 */
_cleanStack.Config
/**
 * Replace the absolute path to the home directory with the `~`. Default `false`.
 * @type {boolean|undefined}
 */
_cleanStack.Config.prototype.pretty
/**
 * Which modules to ignore in the path. Default `［'pirates'］`.
 * @type {(!Array<string>)|undefined}
 */
_cleanStack.Config.prototype.ignoredModules

/* typal types/api.xml externs */
/**
 * Remove internal Node.JS lines from the error stack traces.
 * @typedef {function(string,!_cleanStack.Config=): string}
 */
_cleanStack.cleanStack
