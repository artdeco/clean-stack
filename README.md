<div align="center">

# @artdeco/clean-stack

[![npm version](https://badge.fury.io/js/%40artdeco%2Fclean-stack.svg)](https://www.npmjs.com/package/@artdeco/clean-stack)
<a href="https://gitlab.com/artdeco/clean-stack/-/commits/master">
  <img src="https://gitlab.com/artdeco/clean-stack/badges/master/pipeline.svg" alt="Pipeline Badge">
</a>

</div>

`@artdeco/clean-stack` is used to remove internal _Node.JS_ lines from error stacks, as well as lines from specific modules.

```sh
yarn add @artdeco/clean-stack
npm i @artdeco/clean-stack
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`cleanStack(stack, options=): string`](#cleanstackstack-stringoptions-config-string)
  * [`Config`](#type-config)
- [Copyright & License](#copyright--license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

## API

The package is available by importing its default function:

```js
import cleanStack from '@artdeco/clean-stack'
```

## <code><ins>cleanStack</ins>(</code><sub><br/>&nbsp;&nbsp;`stack: string,`<br/>&nbsp;&nbsp;`options=: !Config,`<br/></sub><code>): <i>string</i></code>
Remove internal Node.JS lines from the error stack traces.

 - <kbd><strong>stack*</strong></kbd> <em>`string`</em>: The stack to clean.
 - <kbd>options</kbd> <em><code><a href="#type-config" title="Options for the program.">!Config</a></code></em> (optional): Additional options.

__<a name="type-config">`Config`</a>__: Options for the program.


|      Name      |             Type              |                          Description                          |    Default    |
| -------------- | ----------------------------- | ------------------------------------------------------------- | ------------- |
| pretty         | <em>boolean</em>              | Replace the absolute path to the home directory with the `~`. | `false`       |
| ignoredModules | <em>!Array&lt;string&gt;</em> | Which modules to ignore in the path.                          | `［'pirates'］` |

The example below will remove unuseful internal _Node.JS_ lines from the error stack. Any other modules to ignore can be passed in the `ignoreModules` option.

```js
/* yarn example/ */
import cleanStack from '@artdeco/clean-stack'

const err = `
Error: test
    at Object.<anonymous> (/Users/zavr/adc/clean-stack/i2.js:1:69)
    at Module._compile (module.js:652:30)
    at Module._compile (/Users/zavr/adc/clean-stack/node_modules/pirates/lib/index.js:83:24)
    at Module._extensions..js (module.js:663:10)
    at Object.newLoader [as .js] (/Users/zavr/adc/clean-stack/node_modules/pirates/lib/index.js:88:7)
    at Module.load (module.js:565:32)
    at tryModuleLoad (module.js:505:12)
    at Function.Module._load (module.js:497:3)
    at Module.require (module.js:596:17)
    at require (internal/module.js:11:18)
`.trim()

const res = cleanStack(err)
```

```
Error: test
    at Object.<anonymous> (/Users/zavr/adc/clean-stack/i2.js:1:69)
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>

## Copyright & License

[Original work](https://www.npmjs.com/package/clean-stack) by Sindre Sorhus under [MIT License](COPYING).

<table>
  <tr>
    <th>
      <a href="https://www.artd.eco">
        <img width="100" src="https://gitlab.com/uploads/-/system/group/avatar/7454762/artdeco.png"
          alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a>   2020</th>
    <th><a href="LICENSE"><img src=".documentary/agpl-3.0.svg" alt="AGPL-3.0"></a></th>
  </tr>
</table>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>