import { homedir } from 'os'

const extractPathRegex = /\s+at.*(?:\(|\s)(.*)\)?/
const pathRegex = /^(?:(?:(?:node|(?:internal\/[\w/]*|.*node_modules\/(?:IGNORED_MODULES)\/.*)?\w+)\.js:\d+:\d+)|native)/

/**
 * @type {_cleanStack.cleanStack}
 */
const cleanStack = (stack, options = {}) => {
  const {
    pretty = false, ignoredModules = ['pirates', '@artdeco/pirates'],
  } = options
  const j = ignoredModules.join('|')
  const re = new RegExp(pathRegex.source.replace('IGNORED_MODULES', j))

  return stack.replace(/\\/g, '/')
    .split('\n')
    .filter(x => {
      const pathMatches = x.match(extractPathRegex)
      if (pathMatches === null || !pathMatches[1]) {
        return true
      }

      const match = pathMatches[1]

      // Electron
      if (match.includes('.app/Contents/Resources/electron.asar') ||
        match.includes('.app/Contents/Resources/default_app.asar')) {
        return false
      }

      return !re.test(match)
    })
    .filter(x => x.trim())
    .map(x => {
      if (pretty) {
        const h = homedir().replace(/\\/g, '/')
        return x.replace(/\s+at.*(?:\(|\s)(.*)\)?/, (m, p1) => m.replace(p1, p1.replace(h, '~')))
      }

      return x
    })
    .join('\n')
}

export default cleanStack

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('..')} _cleanStack.cleanStack
 */