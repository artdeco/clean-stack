## API

The package is available by importing its default function:

```js
import cleanStack from '@artdeco/clean-stack'
```

<typedef noArgTypesInToc name="cleanStack">types/api.xml</typedef>

<typedef>types/index.xml</typedef>

The example below will remove unuseful internal _Node.JS_ lines from the error stack. Any other modules to ignore can be passed in the `ignoreModules` option.

%EXAMPLE: example, ../src => @artdeco/clean-stack%

%FORK example%

%~%