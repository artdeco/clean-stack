<div align="center">

# @artdeco/clean-stack

%NPM: @artdeco/clean-stack%
<pipeline-badge />

</div>

`@artdeco/clean-stack` is used to remove internal _Node.JS_ lines from error stacks, as well as lines from specific modules.

```sh
yarn add @artdeco/clean-stack
npm i @artdeco/clean-stack
```

## Table Of Contents

%TOC%

%~%