import makeTestSuite from '@zoroaster/mask'
import { sep } from 'path'
import { homedir } from 'os'
import cleanStack from '../../src'

const mask = (s) => {
  return s
    // .replace(/\{PATH\}/g, resolve('t.js'))
    .replace(/\{HOME\}/g, (homedir() + sep).replace(/\\/g, '/'))
    .replace(/\{FILE\}/g, 'FILE.js')
}
const rev = (s) => {
  return s
    // .replace(resolve('t.js'), '{PATH}')
    // .replace(homedir() + sep, '{HOME}')
    .replace('FILE.js', '{FILE}')
}

export default makeTestSuite('test/result', {
  getResults() {
    const input = mask(this.input)
    const res = cleanStack(input, this.config)
    return rev(res)
  },
  jsonProps: ['config'],
})